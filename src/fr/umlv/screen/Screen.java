package fr.umlv.screen;

import fr.umlv.zen5.ApplicationContext;
import fr.umlv.zen5.ScreenInfo;

public class Screen {
  private final float width;
  private final float height;
  
  public Screen(ApplicationContext context) {
    // Get the size of the screen.
    ScreenInfo screenInfo = context.getScreenInfo();
    this.width = screenInfo.getWidth();
    this.height = screenInfo.getHeight();
  }

  public float getWidth() {
    return width;
  }

  public float getHeight() {
    return height;
  }

}
