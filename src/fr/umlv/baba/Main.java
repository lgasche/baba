package fr.umlv.baba;

import java.awt.Color;
import java.io.IOException;
import java.lang.ref.Cleaner.Cleanable;

import fr.umlv.baba.Game.Direction;
import fr.umlv.baba.gamedrawer.GameDrawerAscii;
import fr.umlv.baba.gamedrawer.GameDrawerImage;
import fr.umlv.baba.levelparser.LevelParser;
import fr.umlv.baba.tile.SubTypeTile;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;
import fr.umlv.screen.Screen;
import fr.umlv.zen5.Application;
import fr.umlv.zen5.Event;
import fr.umlv.zen5.Event.Action;
import fr.umlv.zen5.KeyboardKey;

public class Main {

  public static void ascii(Board board) {
    var gameDrawer = new GameDrawerAscii();
    gameDrawer.drawBoard(board);

  }

  public static void gui(Game game) {
    var gameDrawerImage = new GameDrawerImage();
    Application.run(Color.BLACK, context -> {
      var screen = new Screen(context);
      while (true) {
        Event event = context.pollOrWaitEvent(1);
        if (event == null) {
          continue;
        }
        // Input != null.
        Action action = event.getAction();
        KeyboardKey key = event.getKey();
        if (action == Action.KEY_PRESSED) {
          var dir = game.inputZen5IntoDirection(key);
          if (dir != null) {
            // Clean the Background.
            context.renderFrame(graphics -> {
              graphics.clearRect(0, 0, (int)screen.getWidth(), (int)screen.getHeight());
            });
            // Check player input and game status.
            var statusGame = game.playDir(dir);
            switch (statusGame) {
              case WIN:
                System.out.print("Vous avez gagné.");
                context.exit(0);
                break;
              case DEFEAT:
                System.out.print("Vous avez perdu.");
                context.exit(0);
                break; 
            }
          }
          // Draw the board.
          var resizeImgW = (int)screen.getWidth() / game.getBoard().getSizeX();
          var resizeImgH = (int)screen.getHeight() / game.getBoard().getSizeY();
          gameDrawerImage.drawBoard(game, context, resizeImgW, resizeImgH);          
          if(key == KeyboardKey.UNDEFINED){
            context.exit(0);    
          }
        }
      }
    });

  }

  public static void main(String[] args) throws IOException {
    var levelParser = new LevelParser();
    var level = levelParser.levelParser("/Baba/data/levels/puzzles/level1.txt");
    var game = new Game(level);
    gui(game);
  }

}
