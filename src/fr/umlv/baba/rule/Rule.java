package fr.umlv.baba.rule;

import fr.umlv.baba.tile.Tile;

public record Rule(Tile noun, Tile operator, Tile property) {

}
