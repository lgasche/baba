package fr.umlv.baba.rule;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.umlv.baba.Board;
import fr.umlv.baba.tile.SubTypeTile;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;

import static java.util.Objects.requireNonNull;

public class RuleParser {

  /**
   * Browse the game board, filter tile OPERATORS.
   * And add them to the HashMap.
   * @param board
   */
  private HashMap<Point, Tile> findAndAddOperators(Board board) {
    requireNonNull(board);
    var operators = new HashMap<Point, Tile>();
    for (var y = 0; y < board.getSizeY(); y++) {
      for (var x = 0; x < board.getSizeX(); x++) {
        for (Tile tile : board.getLayers(x, y)) {
          // Find and add all operators.
          if (tile != null && tile.typeTile().equals(TypeTile.OPERATOR)) {
            operators.put(new Point(x, y), tile);
          }
        }
      }
    }
    return operators;
  }
  
  /**
   * Checks in the layers if it contains a NOUN or a PROPERTY.
   * @param Tile[] layers
   * @return if yes return the tile, else a tile(VOID, BACKGROUND)
   */
  private Tile findInLayer(Tile[] layers) {
    requireNonNull(layers);
    for (Tile tile : layers) {
      if (tile != null) {
        if (tile.typeTile().equals(TypeTile.PROPERTY) ||
            tile.typeTile().equals(TypeTile.NOUN)) {
          return tile;
        }
      }
    }
   return new Tile(TypeTile.VOID, SubTypeTile.BACKGROUND);
  }
  
  /**
   * Check for each operator if it is surrounded by Textile which forms a rule.
   * If yes add the rule in the ArrayList rules, otherwise no.
   * @param x
   * @param y
   * @param op
   * @param board
   */
  private void addRules(int x, int y, Tile op, Board board, ArrayList<Rule> rules) {
    requireNonNull(op);
    requireNonNull(board);
    requireNonNull(rules);
    // UP DOW
    if (y > 0 && y < board.getSizeY()) {
      var up = findInLayer(board.getLayers(x, y-1));
      if (up.typeTile().equals(TypeTile.NOUN)) {
        var down = findInLayer(board.getLayers(x, y+1));
        if (!down.typeTile().equals(TypeTile.VOID)) {
          rules.add(new Rule(up, op, down));
        }
      }
    }
    // LEFT RIGHT
    if (x > 0 && x < board.getSizeX()) {
      var left = findInLayer(board.getLayers(x-1, y));
      if (left.typeTile().equals(TypeTile.NOUN)) {
        var right = findInLayer(board.getLayers(x+1, y));
        if (!right.typeTile().equals(TypeTile.VOID)) {
          rules.add(new Rule(left, op, right));
        }
      }
    }
  }

  /**
   * Parse the board and find the rule. 
   * @param 
   * @return ArrayList<Rule> of rule
   */
  public ArrayList<Rule> ruleParser(Board board, ArrayList<Rule> rules) {
    requireNonNull(board);
    requireNonNull(rules);
    rules.clear();
    var operators = findAndAddOperators(board);
    for (Map.Entry<Point, Tile> entry : operators.entrySet()) {
      var x = (int)entry.getKey().getX();
      var y = (int)entry.getKey().getY();
      var op = entry.getValue();
      addRules(x, y, op, board, rules);
    }
    return rules;
  }

}
