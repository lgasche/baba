package fr.umlv.baba.levelparser;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import fr.umlv.baba.Board;
import fr.umlv.baba.tile.TileFactory;

public class LevelParser {
  
  /**
   * Parse the level.txt and init the board.
   * @param filePath
   * @throws IOException
   */
  public Board levelParser(String filePath) throws IOException {
    File file = new File(filePath);
    var reader = new Scanner(file);
    var sizeX = Integer.parseInt(reader.next());
    var sizeY = Integer.parseInt(reader.next());
    var board = new Board(sizeX, sizeY);
    var tileFactory = new TileFactory();
    // Set the reader at the start of row.
    for(int y = 0; reader.hasNext(); y++) {
      // Read the row and cut it in char (code for tile).
      var ref = reader.next();
      for(var x = 0; x < ref.length(); x++) {
        // Init a tile, with the code associate.
        // Chose the layer z to put the tile.
        board.setTile(tileFactory.getTile(String.valueOf(ref.charAt(x))), x, y, 0);
      }
    }
    reader.close();
    return board;
  }

}
