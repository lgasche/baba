package fr.umlv.baba.gamedrawer;

import static java.util.Objects.requireNonNull;

import fr.umlv.baba.Board;
import fr.umlv.baba.Game;
import fr.umlv.baba.tile.SubTypeTile;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;
import fr.umlv.zen5.ApplicationContext;

public class GameDrawerAscii implements TileView {
  
  public void drawTile(Tile tile) {
    requireNonNull(tile);
    // PlayableTile.
    if (tile.typeTile().equals(TypeTile.PLAYABLE)) {
      switch (tile.subTypeTile()) {
        case BABA:
          System.out.print("b");
          break;
        case FLAG:
          System.out.print("f");
          break;
        case WALL:
          System.out.print("w");
          break;
        case ROCK:
          System.out.print("r");
          break;  
      }
    }
    // TextTiles.
    if (tile.typeTile().equals(TypeTile.NOUN)) {
      switch (tile.subTypeTile()) {
        case BABA:
          System.out.print("B");
          break;
        case FLAG:
          System.out.print("F");
          break;
        case WALL:
          System.out.print("W");
          break;
        case ROCK:
          System.out.print("R");
          break;  
      } 
    }
    if (tile.typeTile().equals(TypeTile.OPERATOR)) {
      switch (tile.subTypeTile()) {
        case IS:
          System.out.print("I");
          break;
      }
    }
    if (tile.typeTile().equals(TypeTile.PROPERTY)) {
      switch (tile.subTypeTile()) {
        case PUSH:
          System.out.print("P");
          break;
        case WIN:
          System.out.print("G");
          break;
        case STOP:
          System.out.print("S");
          break;
        case YOU:
          System.out.print("Y");
          break;  
      }
    }
    // Default.
    if (tile.typeTile().equals(TypeTile.VOID)) {
      switch (tile.subTypeTile()) {
        case BACKGROUND:
          System.out.print("n");
          break; 
      }
    }
  }
  
  public void drawBoard(Board board) {
    for (var y = 0; y < board.getSizeY(); y++) {
      for (var x = 0; x < board.getSizeX(); x++) {
        for (Tile tile : board.getLayers(x, y)) {
          if (tile != null) {
            drawTile(tile);
          }
        }
      }
      System.out.println();
    }
  }
  
  // Because it needs to be here.
  public void drawTile(Tile tile, int x, int y, ApplicationContext context, int resizeImgW, int resizeImgH)  { }
  public void drawBoard(Game game, ApplicationContext context, int resizeImgW, int resizeImgH)  { }
  public void drawBoard(Game game) { }
  
}
