package fr.umlv.baba.gamedrawer;

import static java.util.Objects.requireNonNull;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import fr.umlv.baba.Game;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;
import fr.umlv.zen5.ApplicationContext;

public class GameDrawerImage implements TileView {
  
  /**
   * Resize the BufferedImage src by newH and newH
   * @param src
   * @param newW
   * @param newH
   * @return BufferedImage resize
   */
  private static BufferedImage resize(BufferedImage src, int newW, int newH) {
    var newImg = new BufferedImage(newW, newH, src.getType());
    var x = 0; 
    var y = 0;
    var oldW = src.getWidth();
    var oldH = src.getHeight();
    int[] ys = new int[newH];
    for (y = 0; y < newH; y++)
      ys[y] = y * oldH / newH;
      for (x = 0; x < newW; x++) {
        var newX = x * oldW / newW;
        for (y = 0; y < newH; y++) {
          var col = src.getRGB(newX, ys[y]);
          newImg.setRGB(x, y, col);
        }
      }
    return newImg;
  }
  
  private BufferedImage prepareImage(String path, int resizeImgW, int resizeImgH) {
    BufferedImage inputImg = null;
    try {
      inputImg = ImageIO.read(new File(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
    var outputImg = resize(inputImg, resizeImgW, resizeImgH);
    return outputImg;
  }
  
  private BufferedImage loadImage(Tile tile, int resizeImgW, int resizeImgH) {
    BufferedImage img = null;
    // PlayableTile.
    if (tile.typeTile().equals(TypeTile.PLAYABLE)) {
      switch (tile.subTypeTile()) {
        case BABA:
          img = prepareImage("data/img/playable/BABA.png", resizeImgW, resizeImgH);
          break;
        case FLAG:
          img = prepareImage("data/img/playable/FLAG.png", resizeImgW, resizeImgH);
          break;
        case WALL:
          img = prepareImage("data/img/playable/WALL.png", resizeImgW, resizeImgH);
          break;
        case ROCK:
          img = prepareImage("data/img/playable/ROCK.png", resizeImgW, resizeImgH);
          break; 
        case GRASS:
          img = prepareImage("data/img/playable/GRASS.png", resizeImgW, resizeImgH);
          break;
        case WATER:
          img = prepareImage("data/img/playable/WATER.png", resizeImgW, resizeImgH);
          break;
        case SKULL:
          img = prepareImage("data/img/playable/SKULL.png", resizeImgW, resizeImgH);
          break;
        case LAVA:
          img = prepareImage("data/img/playable/LAVA.png", resizeImgW, resizeImgH);
          break; 
      }
    }
    // TextTiles.
    else if (tile.typeTile().equals(TypeTile.NOUN)) {
      switch (tile.subTypeTile()) {
        case BABA:
          img = prepareImage("data/img/noun/BABA_TEXT.png", resizeImgW, resizeImgH);
          break;
        case FLAG:
          img = prepareImage("data/img/noun/FLAG_TEXT.png", resizeImgW, resizeImgH);
          break;
        case WALL:
          img = prepareImage("data/img/noun/WALL_TEXT.png", resizeImgW, resizeImgH);
          break;
        case ROCK:
          img = prepareImage("data/img/noun/ROCK_TEXT.png", resizeImgW, resizeImgH);
          break; 
        case WATER:
          img = prepareImage("data/img/noun/WATER_TEXT.png", resizeImgW, resizeImgH);
          break;
        case SKULL:
          img = prepareImage("data/img/noun/SKULL_TEXT.png", resizeImgW, resizeImgH);
          break;
        case LAVA:
          img = prepareImage("data/img/noun/LAVA_TEXT.png", resizeImgW, resizeImgH);
          break;  
      } 
    }
    else if (tile.typeTile().equals(TypeTile.OPERATOR)) {
      switch (tile.subTypeTile()) {
        case IS:
          img = prepareImage("data/img/operator/IS_TEXT.png", resizeImgW, resizeImgH);
          break;
      }
    }
    else if (tile.typeTile().equals(TypeTile.PROPERTY)) {
      switch (tile.subTypeTile()) {
        case PUSH:
          img = prepareImage("data/img/property/PUSH_TEXT.png", resizeImgW, resizeImgH); 
          break;
        case WIN:
          img = prepareImage("data/img/property/WIN_TEXT.png", resizeImgW, resizeImgH);
          break;
        case STOP:
          img = prepareImage("data/img/property/STOP_TEXT.png", resizeImgW, resizeImgH);
          break;
        case YOU:
          img = prepareImage("data/img/property/YOU_TEXT.png", resizeImgW, resizeImgH);
          break;  
        case SINK:
          img = prepareImage("data/img/property/SINK_TEXT.png", resizeImgW, resizeImgH);
          break;
        case DEFEAT:
          img = prepareImage("data/img/property/DEFEAT_TEXT.png", resizeImgW, resizeImgH);
          break;
        case MELT:
          img = prepareImage("data/img/property/MELT_TEXT.png", resizeImgW, resizeImgH);
          break;
        case HOT:
          img = prepareImage("data/img/property/HOT_TEXT.png", resizeImgW, resizeImgH);
          break;
      }
    }
    // Default. (tile.typeTile().equals(TypeTile.VOID))
    else if (tile.typeTile().equals(TypeTile.VOID)) {
      switch (tile.subTypeTile()) {
        case BACKGROUND:
          break; 
      }
    }
    return img;
  }
  
  
  private void drawImage(ApplicationContext context, Image img, int x, int y) {
    context.renderFrame(graphics -> {
      graphics.drawImage(img, x, y, null);
    });
  }
  
  public void drawTile(Tile tile, int x, int y, ApplicationContext context, int resizeImgW, int resizeImgH) {
    requireNonNull(tile);
    BufferedImage img = loadImage(tile, resizeImgW, resizeImgH);
    drawImage(context, img, x*resizeImgW, y*resizeImgH);
  }

  public void drawBoard(Game game, ApplicationContext context, int resizeImgW, int resizeImgH) {
    for (var y = 0; y < game.getBoard().getSizeY(); y++) {
      for (var x = 0; x < game.getBoard().getSizeX(); x++) {
        for (Tile tile : game.getBoard().getLayers(x, y)) {
          if (tile != null) {
            drawTile(tile, x, y, context, resizeImgW, resizeImgH);
          }
        }
      }
    }
  }
  
  // Because it needs to be here.
  public void drawTile(Tile tile) { }
  public void drawBoard(Game game) { }
  
}
