package fr.umlv.baba.gamedrawer;

import fr.umlv.baba.Game;
import fr.umlv.baba.tile.Tile;
import fr.umlv.zen5.ApplicationContext;

public interface TileView {
  // ASCII.
  void drawTile(Tile tile);
  void drawBoard(Game game);
  
  // GUI.
  void drawTile(Tile tile, int x, int y, ApplicationContext context, int resizeImgW, int resizeImgH);
  void drawBoard(Game game, ApplicationContext context, int resizeImgW, int resizeImgH);
}
