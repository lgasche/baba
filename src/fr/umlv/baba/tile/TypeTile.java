package fr.umlv.baba.tile;

public enum TypeTile {
  NOUN, OPERATOR, PROPERTY, PLAYABLE, VOID
}
