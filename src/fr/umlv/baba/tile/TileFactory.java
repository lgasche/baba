package fr.umlv.baba.tile;

import static java.util.Objects.requireNonNull;

public class TileFactory {

  public Tile getTile(String ref) {
    requireNonNull(ref);   
    return switch(ref) {
      //  PlayableTiles.
      case "b" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.BABA);
      case "w" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.WALL);
      case "r" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.ROCK);
      case "f" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.FLAG);
      case "u" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.GRASS);
      case "a" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.WATER);
      case "k" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.SKULL);
      case "l" -> new Tile(TypeTile.PLAYABLE, SubTypeTile.LAVA);
      //  TextTiles.
      //    Nouns.
      case "B" -> new Tile(TypeTile.NOUN, SubTypeTile.BABA);
      case "W" -> new Tile(TypeTile.NOUN, SubTypeTile.WALL);
      case "R" -> new Tile(TypeTile.NOUN, SubTypeTile.ROCK);
      case "F" -> new Tile(TypeTile.NOUN, SubTypeTile.FLAG);
      case "A" -> new Tile(TypeTile.NOUN, SubTypeTile.WATER);
      case "K" -> new Tile(TypeTile.NOUN, SubTypeTile.SKULL);
      case "L" -> new Tile(TypeTile.NOUN, SubTypeTile.LAVA);
      //    Operators.
      case "I" -> new Tile(TypeTile.OPERATOR, SubTypeTile.IS);
      //    Properties.
      case "Y" -> new Tile(TypeTile.PROPERTY, SubTypeTile.YOU);
      case "P" -> new Tile(TypeTile.PROPERTY, SubTypeTile.PUSH);
      case "S" -> new Tile(TypeTile.PROPERTY, SubTypeTile.STOP);
      case "G" -> new Tile(TypeTile.PROPERTY, SubTypeTile.WIN);
      case "N" -> new Tile(TypeTile.PROPERTY, SubTypeTile.SINK);
      case "D" -> new Tile(TypeTile.PROPERTY, SubTypeTile.DEFEAT);
      case "M" -> new Tile(TypeTile.PROPERTY, SubTypeTile.MELT);
      case "H" -> new Tile(TypeTile.PROPERTY, SubTypeTile.HOT);
      //  DefaultTile (nothing).
      default -> new Tile(TypeTile.VOID, SubTypeTile.BACKGROUND);
    };
  }
  
}
