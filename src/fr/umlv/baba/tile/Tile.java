package fr.umlv.baba.tile;


@SuppressWarnings("preview")
public record Tile(TypeTile typeTile, SubTypeTile subTypeTile) {

  @Override
  public String toString() {
    return subTypeTile.toString() + " ";
  }
  
}

