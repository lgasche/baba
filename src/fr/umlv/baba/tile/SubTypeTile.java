package fr.umlv.baba.tile;

public enum SubTypeTile {
  BACKGROUND, 
  // Nouns.
  BABA, FLAG, WALL, ROCK, GRASS, WATER, SKULL, LAVA,
  // Operators.
  IS,
  // Property.
  YOU, WIN, STOP, PUSH, SINK, DEFEAT, MELT, HOT,
  // Other.
  MOVE
}
