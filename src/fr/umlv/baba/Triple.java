package fr.umlv.baba;

/**
 * 3 dimensional representation, x and y for the position in the board, z in the layers.
 */
public record Triple(int x, int y, int z) {
  
}
