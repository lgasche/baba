package fr.umlv.baba;

import java.util.Comparator;

import fr.umlv.baba.Game.Direction;
import static java.util.Objects.requireNonNull;

public class TripleComparator implements Comparator<Triple> {
  private final Direction d;
  
  public TripleComparator(Direction d) {
    this.d = d;
  }

  // Smaller y first.
  private int compareUp(Triple a, Triple b) {
    requireNonNull(a);
    requireNonNull(b);
    return a.y() < b.y() ? -1 : a.y() == b.y() ? 0 : 1;
  }
  
  // Biggest x first.
  private int compareRight(Triple a, Triple b) {
    requireNonNull(a);
    requireNonNull(b);
    return a.x() > b.x() ? -1 : a.x() == b.x() ? 0 : 1;
  }
  
  // Bigger y first.
  private int compareDown(Triple a, Triple b) {
    requireNonNull(a);
    requireNonNull(b);
    return a.y() > b.y() ? -1 : a.y() == b.y() ? 0 : 1;
  }
  
  // Smallest x first.
  private int compareLeft(Triple a, Triple b) {
    requireNonNull(a);
    requireNonNull(b);
    return a.x() < b.x() ? -1 : a.x() == b.x() ? 0 : 1;
  }
  
  @Override
  public int compare(Triple o1, Triple o2) {
    return switch(d) {
      case UP -> compareUp(o1, o2);
      case RIGHT -> compareRight(o1, o2);
      case DOWN -> compareDown(o1, o2);
      case LEFT -> compareLeft(o1, o2);
    };
  }

}
