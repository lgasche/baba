package fr.umlv.baba;

import java.awt.Point;
import java.util.ArrayList;

import fr.umlv.baba.Game.Direction;
import fr.umlv.baba.rule.Rule;
import fr.umlv.baba.tile.SubTypeTile;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;
import static java.util.Objects.requireNonNull;

public class Board {
  private static final int NBLAYERS = 3;
  private final Tile[][][] board;
  private final int sizeX;
  private final int sizeY;

  public Board(int sizeX, int sizeY) {
    this.board = new Tile[sizeX][sizeY][NBLAYERS];
    this.sizeX = sizeX;
    this.sizeY = sizeY;
  }
  
  /**
   * Set a tile in the board at the position board[x][y][z].
   * Method use in parseLevel for creating the board.
   * @param tile
   * @param x
   * @param y
   * @param z
   */
  public void setTile(Tile tile, int x, int y, int z) {
    if (x > sizeX || y > sizeY || z > NBLAYERS || x < 0 || y < 0 || z < 0) {
      throw new IllegalArgumentException("Position out of bound.");
    }
    board[x][y][z] = tile;
  }
  
  public int getSizeX() {
    return sizeX;
  }
  
  public int getSizeY() {
    return sizeY;
  }
  
  public int getSizeZ() {
    return NBLAYERS;
  }
  
  public Tile[] getLayers(int x, int y) {
    if (x > sizeX || y > sizeY || x < 0 || y < 0) {
      throw new IllegalArgumentException("Position out of bound.");
    }
    return board[x][y];
  }

  /**
   * Check all rules related to the tile type noun.
   * @return ArrayList<SubTypeTile> of property.
   */
  private ArrayList<SubTypeTile> checkStatus(Tile tile, ArrayList<Rule> rules) {
    requireNonNull(tile);
    requireNonNull(rules);
    var status = new ArrayList<SubTypeTile>() ;
    for (Rule rule : rules) {
      if (rule.noun().subTypeTile().equals(tile.subTypeTile())) {
        status.add(rule.property().subTypeTile());
      }          
    }
    return status;
  }
  
  /**
   * Takes as parameters a coordinate source (x, y) and a direction.
   * @param x
   * @param y
   * @param d
   * @return Point(x, y) with layers destination coordinates.
   */
  private Point returnLayersDst(int x, int y, Direction d) {
    requireNonNull(d);
    if (x > sizeX || y > sizeY || x < 0 || y < 0) {
      throw new IllegalArgumentException("Position out of bound.");
    }
    switch(d) {
      case UP:
        y--;
        break;
      case RIGHT:
        x++;
        break;
      case DOWN:
        y++;
        break;
      case LEFT:
        x--;
        break;
    }
    return new Point(x, y);
  }
  
  /**
   * @param layers
   * @return true if the layers contain a textile, otherwise return false.
   */
  private boolean containsTextTile(Tile[] layers) {
    requireNonNull(layers);
    for (var i = 0; i < layers.length && layers[i] != null; i++) {
      if (layers[i].typeTile().equals(TypeTile.NOUN)     ||
          layers[i].typeTile().equals(TypeTile.OPERATOR) ||
          layers[i].typeTile().equals(TypeTile.PROPERTY)) {
        return true;
      }
    }
    return false;
  }
  
  
  /**
   * @param x
   * @param y
   * @param d
   * @param rules
   * @return if SubTypeTile.MOVE, if not return a status of type SubTypeTile.
   */
  public SubTypeTile canMoveTo(int x, int y, Direction d, ArrayList<Rule> rules) {
    requireNonNull(rules);
    requireNonNull(d);
    if (x > sizeX || y > sizeY || x < 0 || y < 0) {
      throw new IllegalArgumentException("Position out of bound.");
    }
    // Out of bounds.
    if (y-1 < 0 && d.equals(Direction.UP)) {
      return SubTypeTile.STOP;
    }
    if (y+1 >= sizeY && d.equals(Direction.DOWN)) {
      return SubTypeTile.STOP;
    }
    if (x-1 < 0 && d.equals(Direction.LEFT)) {
      return SubTypeTile.STOP;
    }
    if (x+1 >= sizeX && d.equals(Direction.RIGHT)) {
      return SubTypeTile.STOP;
    }
    // Status.
    var coordDst = returnLayersDst(x, y, d);
    var layersDst = board[(int)coordDst.x][(int)coordDst.y];
    // Case layers contains a TextTile.
    if (containsTextTile(layersDst)) {
      return SubTypeTile.PUSH;
    }
    // Check all tiles in layers, for find a status.
    for (var tileInLayersDst : layersDst) {
      if (tileInLayersDst != null) {
        var status = checkStatus(tileInLayersDst, rules);
        if (status.contains(SubTypeTile.STOP)) {
          // Do noting.
          return SubTypeTile.STOP;
        }
        if (status.contains(SubTypeTile.PUSH)) {
          // Push the tile and player.
          return SubTypeTile.PUSH;
        }
        if (status.contains(SubTypeTile.WIN)) {
          // End game.
          return SubTypeTile.WIN;
        }
      }
    }
    // Default.
    return SubTypeTile.MOVE;
  }
  
}
