package fr.umlv.baba;

import java.util.ArrayList;

import fr.umlv.baba.rule.Rule;
import fr.umlv.baba.rule.RuleParser;
import fr.umlv.baba.tile.Tile;
import fr.umlv.baba.tile.TypeTile;
import fr.umlv.zen5.KeyboardKey;
import fr.umlv.baba.tile.SubTypeTile;
import static java.util.Objects.requireNonNull;

public class Game {
  private final Board board;
  private final ArrayList<Rule> rules;
  private final ArrayList<Triple> playerTiles;

  public Game(Board board) {
    this.board = board;
    this.rules = new ArrayList<Rule>();
    this.playerTiles = new ArrayList<Triple>();
  }
  
  public Board getBoard() {
    return board;
  }
  
  /**
   * Browse the rules, and return the TypeTile associated with the SubTypeTile.YOU.
   * @return TypeTile associated with the SubTypeTile.YOU
   */
  private Tile getYou() {
    Tile player = null;
    for (Rule rule : rules) {
      if (rule.property().subTypeTile().equals(SubTypeTile.YOU)) {
        player = rule.noun();
      }          
    }
    return player;
  }
  
  /**
   * Clear the arrayList of players.
   * Find all the tiles associated with the Time.YOU SubType.
   * Add their x, y, z coordinates in a Triple(x, y, z) dimensions type.
   * Add all Triple in à arrayList of players.
   */
  private void findAndAddPlayers() {
    playerTiles.clear();
    var player = getYou();
    for (var y = 0; y < board.getSizeY(); y++) {
      for (var x = 0; x < board.getSizeX(); x++) {
        var z = 0;
        for (Tile tile : board.getLayers(x, y)) {
          // Find and add all players.
          if (tile != null &&
              tile.typeTile().equals(TypeTile.PLAYABLE) &&
              tile.subTypeTile().equals(player.subTypeTile())) {
            playerTiles.add(new Triple(x, y, z));
          }
          z++;
        }
      }
    }
  }

  /**
   * Check if a rule have a property as YOU.
   * Else game over.
   */
  public boolean checkRuleYou() {
    for (Rule rule : rules) {
      if (rule.property().subTypeTile().equals(SubTypeTile.YOU)) {
        return true;
      }          
    }
    return false;
  }
  
  /**
   * Enum use to transition with zen library 5.
   */
  enum Direction {
    UP, RIGHT, LEFT, DOWN
  }
  
  /**
   * Interface between the inputs of the zen5 library and that of the game.
   * @param key
   * @return enum Direction 
   */
  public Direction inputZen5IntoDirection(KeyboardKey key) {
    requireNonNull(key);
    if (key.equals(KeyboardKey.UP)) {
      return Direction.UP;
    }
    if (key.equals(KeyboardKey.RIGHT)) {
      return Direction.RIGHT;
    }
    if (key.equals(KeyboardKey.DOWN)) {
      return Direction.DOWN;
    }
    if (key.equals(KeyboardKey.LEFT)) {
      return Direction.LEFT;
    }
    return null;
  }
  
  /**
   * Add a tile in a layers.
   * @param layers
   * @param tile
   */
  private void addInLayers(Tile[] layers, Tile tile) {
    requireNonNull(layers);
    requireNonNull(tile);
    for (var i = 0; i < layers.length; i++) {
      if (layers[i] == null) {
        layers[i] = tile;
        return;
      }
    }
  }
  
  /**
   * Remove a tile in a layers.
   * @param layers
   * @param tile
   */
  private void removeInlayers(Tile[] layers, Tile tile) {
    requireNonNull(layers);
    requireNonNull(tile);
    for (var i = layers.length - 1; i >= 0; i--) {
      if (tile.equals(layers[i])) {
        layers[i] = null;      
        return;
      }
    }
  }
  
  /**
   * @param x
   * @param y
   * @param d
   * @return layers of the direction.
   */
  private Tile[] returnLayersDst(int x, int y, Direction d) {
    requireNonNull(d);
    switch(d) {
      case UP:
        y--;
        break;
      case RIGHT:
        x++;
        break;
      case DOWN:
        y++;
        break;
      case LEFT:
        x--;
        break;
    }
    return board.getLayers(x, y);
  }
  
  /**
   * Takes a tile, if it is not present in the destination layers, we add it, otherwise we do not add it.
   * Remove the tile of the source layers.
   * @param tilePos
   * @param layersDst
   */
  private void move(Triple tilePos, Tile[] layersDst) {
    requireNonNull(tilePos);
    requireNonNull(layersDst);
    // Take the tile to move and its layers.
    var layersSrc = board.getLayers(tilePos.x(), tilePos.y());
    var tile = layersSrc[tilePos.z()];
    // Check if the tile is is already in the layers.
    if (!contains(layersDst, tile.typeTile(), tile.subTypeTile())) {
      addInLayers(layersDst, tile);
    }
    removeInlayers(layersSrc, tile);
  }
  
  /**
   * Checks if a tile is present in the layers.
   * @param layers
   * @param typeTile
   * @param subTypeTile
   * @return true, otherwise false.
   */
  private boolean contains(Tile[] layers, TypeTile typeTile, SubTypeTile subTypeTile) {
    requireNonNull(layers);
    requireNonNull(typeTile);
    requireNonNull(subTypeTile);
    for (var i = 0; i < layers.length && layers[i] != null; i++) {
      if (layers[i].typeTile().equals(typeTile) && layers[i].subTypeTile().equals(subTypeTile)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Takes the direction entered by the player.
   * Analyze the rules of the table.
   * Sort player tiles according to their position to avoid collisions.
   * Checks if the move is valid.
   * @param d
   */
  public SubTypeTile playDir(Direction d) {
    requireNonNull(d);
    var ruleParser = new RuleParser();
    ruleParser.ruleParser(board, rules);
    findAndAddPlayers();
    // Sort array by direction.
    var tripleComparator = new TripleComparator(d);
    playerTiles.sort(tripleComparator);
    // For all tiles that are the players.
    for (var playerTilePos : playerTiles) {
      // Check status of potential move.
      var status = board.canMoveTo(playerTilePos.x(), playerTilePos.y(), d, rules);
      switch (status) {
        case STOP:
          // Do noting.
          break;
        case WIN:
          return SubTypeTile.WIN;
        case PUSH:
          // Call function push()
          break;
        case MOVE:
          var layersDst = returnLayersDst(playerTilePos.x(), playerTilePos.y(), d);
          move(playerTilePos, layersDst);
          break;
        case MELT:
          // Call function melt().
          break;
        case HOT:
          // Call function hot().
          break;
        case DEFEAT:
          return SubTypeTile.DEFEAT;
      }
    }
    return SubTypeTile.MOVE;
  }
  
  public void showPlayerTiles() {
    for (var playerTile : playerTiles) {
      System.out.println(String.valueOf(playerTile.x()) + " " + String.valueOf(playerTile.y()) + " " + String.valueOf(playerTile.z()));
    }
  }

  public void showRules() {
    rules.forEach((rule) -> System.out.println(rule.toString()));
  }
  
}
